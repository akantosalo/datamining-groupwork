#!/usr/bin/env python
# -*- coding: utf8 -*-
import csv
import collections
import scipy
import datetime
import apriori
import sys
import itertools
import pylab
from scipy import stats
import matplotlib.pyplot as plt
from pylab import hist, show,subplot

def readData(fileName):
    # Courses will be tuples with this names
    Course = collections.namedtuple('Course', 'period code name credits grade')
    # Students will be tuples with this names
    Student = collections.namedtuple('Student', 'registration courses')
    ret = list()
    with open(fileName, 'rb') as dataFile:
        # We consider it to be a csv using space as separator
        datum = list(csv.reader(dataFile, delimiter=' '))
        for d in datum:
            # We perform a sanity check to ensure each course has 5 fields
            assert((len(d)-1) % 5 == 0)
            courses = list()
            for i in range((len(d) -1) / 5):
                # Every 5 entries make a record
                # TODO build objects from the values (dates, integers and so on)
                #: courses.append(Course(*d[i*5+1:i*5+6]))
                courses.append(Course(datetime.datetime.strptime(d[i*5+1], '%Y-%m').date(), d[i*5+2], d[i*5+3], float(d[i*5+4]), float(d[i*5+5])))
            ret.append(Student(datetime.datetime.strptime(d[0], '%Y').date(), courses))
    return ret

datum = readData('data.txt')

#CourseNames
courseNames = dict(                                    #Make a dictionary
	set(itertools.chain(*                              #Of the flatlist
	map(                                               #Of maping
		lambda student: set(map(                       #Each student to the set
			lambda course: (course.code, course.name), #Of pairs (code, name)
			student.courses)),                         #For each course
		datum                                          #For each student
))))

autumn_mean = []
spring_mean = []

datum = readData('data.txt')

for student in datum:
    autumnMean = float(0)
    autumnCredits = float(0)
    springMean = float(0)
    springCredits = float(0)
    for course in student.courses:
        #print course
        month = course.period.month
        credits = float(course.credits)
        grade = float(course.grade)
        if credits >0:
            if month <7:          
                autumnMean = (autumnMean*autumnCredits+credits*grade)/(autumnCredits+credits)
                autumnCredits = autumnCredits + credits

            else:
                springMean = (springMean*springCredits+grade*credits)/(springCredits+credits)
                springCredits = springCredits + credits
    if(autumnMean >0 and springMean >0):
        autumn_mean.append(autumnMean)
        spring_mean.append(springMean)

shapiroa = stats.shapiro(autumn_mean)
print "Shapiro test for autumn: ",shapiroa



shapiros = stats.shapiro(spring_mean)
print "Shapiro test for spring: ",shapiros

test = stats.ttest_rel(autumn_mean, spring_mean)


print "Autumn mean: ",reduce(lambda x, y: x + y, autumn_mean) / len(autumn_mean)
print "Spring mean: ",reduce(lambda x, y: x + y, spring_mean) / len(spring_mean)
print "T-test: ",test

autumn_mean_zero = []
spring_mean_zero = []

for student in datum:
    autumnMean = float(0)
    autumnCredits = float(0)
    springMean = float(0)
    springCredits = float(0)
    uniqueCourses = []
    for course in student.courses:
        grade = course.grade
        code = course.code
        previous = filter(lambda x: x.code==code, uniqueCourses)
        #print previous
        if previous:
            if(previous[0].grade<grade):
                uniqueCourses.remove(previous[0])
                uniqueCourses.append(course)
        else:
            uniqueCourses.append(course)
    #print uniqueCourses
    for course in uniqueCourses:
        #print course
        month = course.period.month
        credits = float(course.credits)
        grade = float(course.grade)
        if credits >0:
            if month <7:          
                autumnMean = (autumnMean*autumnCredits+credits*grade)/(autumnCredits+credits)
                autumnCredits = autumnCredits + credits

            else:
                springMean = (springMean*springCredits+grade*credits)/(springCredits+credits)
                springCredits = springCredits + credits
    if(autumnMean >0 and springMean >0):
        autumn_mean_zero.append(autumnMean)
        spring_mean_zero.append(springMean)

shapiroa_zero = stats.shapiro(autumn_mean_zero)




print "Shapiro test for autumn without redos: ",shapiroa_zero



shapiros_zero = stats.shapiro(spring_mean_zero)
print "Shapiro test for spring without redos: ",shapiros_zero

test_zero = stats.ttest_rel(autumn_mean_zero, spring_mean_zero)

plt.hist(autumn_mean_zero)
plt.hist(spring_mean_zero)
print "Autumn mean: ",reduce(lambda x, y: x + y, autumn_mean_zero) / len(autumn_mean_zero)
print "Spring mean: ",reduce(lambda x, y: x + y, spring_mean_zero) / len(spring_mean_zero)
print "T-test without redos: ",test_zero 

subplot(221)
stats.probplot(autumn_mean, dist="norm", plot=pylab)
subplot(222)
stats.probplot(spring_mean, dist="norm", plot=pylab)
subplot(223)
stats.probplot(autumn_mean_zero, dist="norm", plot=pylab)
subplot(224)
stats.probplot(spring_mean_zero, dist="norm", plot=pylab)
pylab.show()

subplot(221)
hist(autumn_mean, normed=False, bins=30)
subplot(222)
hist(spring_mean, normed=False, bins=30)
subplot(223)
hist(autumn_mean_zero, normed=False, bins=30)
subplot(224)
hist(spring_mean_zero, normed=False, bins=30)
show()

autumn_mean_treshold = []
spring_mean_treshold = []
treshold = 29
for student in datum:
    autumnMean = float(0)
    autumnCredits = float(0)
    springMean = float(0)
    springCredits = float(0)
    uniqueCourses = []
    for course in student.courses:
            
        #grade = course.grade
        #code = course.code
        #previous = filter(lambda x: x.code==code, uniqueCourses)
        #print previous
        #if previous:
        #    if(previous[0].grade<grade):
        #        uniqueCourses.remove(previous[0])
        #        uniqueCourses.append(course)
        #else:
        #    uniqueCourses.append(course)
    #print uniqueCourses
    #for course in uniqueCourses:
        
        month = course.period.month
        credits = float(course.credits)
        grade = float(course.grade)
        if credits >0:
            if month <7:          
                autumnMean = (autumnMean*autumnCredits+credits*grade)/(autumnCredits+credits)
                autumnCredits = autumnCredits + credits

            else:
                springMean = (springMean*springCredits+grade*credits)/(springCredits+credits)
                springCredits = springCredits + credits
    if(autumnMean >0 and springMean >0 and autumnCredits>treshold and springCredits>treshold):
        autumn_mean_treshold.append(autumnMean)
        spring_mean_treshold.append(springMean)

print len(autumn_mean_treshold)
print len(spring_mean_treshold)

shapiroa_treshold = stats.shapiro(autumn_mean_treshold)




print "Shapiro test for autumn with treshold: ",shapiroa_treshold 



shapiros_treshold = stats.shapiro(spring_mean_treshold)
print "Shapiro test for spring with treshold: ",shapiros_treshold 

test_treshold = stats.ttest_rel(autumn_mean_treshold, spring_mean_treshold)

subplot(221)
stats.probplot(autumn_mean, dist="norm", plot=pylab)
subplot(222)
stats.probplot(spring_mean, dist="norm", plot=pylab)
subplot(223)
stats.probplot(autumn_mean_treshold, dist="norm", plot=pylab)
subplot(224)
stats.probplot(spring_mean_treshold, dist="norm", plot=pylab)
pylab.show()

subplot(221)
hist(autumn_mean, normed=False, bins=30)
subplot(222)
hist(spring_mean, normed=False, bins=30)
subplot(223)
hist(autumn_mean_treshold, normed=False, bins=30)
subplot(224)
hist(spring_mean_treshold, normed=False, bins=30)
show()

print "Autumn mean: ",reduce(lambda x, y: x + y, autumn_mean_treshold) / len(autumn_mean_treshold)
print "Spring mean: ",reduce(lambda x, y: x + y, spring_mean_treshold) / len(spring_mean_treshold)
print "T-test with treshold: ",test_treshold 

