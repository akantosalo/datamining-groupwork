#!/usr/bin/env python
# -*- coding: utf8 -*-
import itertools

#We need some verbosity value
verbose = False

#We will define support a litle differently so that we can 
#compute things faster. Support now will stand for
#"records that would be counted when computing the suppport"
def supportList(s, data):
	return filter(lambda e: set(s).issubset(set(e)), data)


#Make the next generation, given a sorted list of parents
def aprioriSetGen(parents):
	candidates = list()
	i = 0
	while i < len(parents):
		j = i+1
		while j < len(parents) and parents[i][0][:-1] == parents[j][0][:-1]:
			#We will use only two slots of the list
			new    = [None, None]
			#The candidate will be all the items from the first plus the last one from the second
			new[0] = sorted(list(set(parents[i][0] + [parents[j][0][-1]])))
			#As candidates for support, we use the smallest list of the two to compute the next list
			#Then we filter with the missing item
			if len(parents[i][1]) < len(parents[j][1]):
				new[1] = supportList([parents[j][0][-1]], parents[i][1])
			else:
				new[1] = supportList([parents[i][0][-1]], parents[j][1])
			#We add the new candidate to our candidate pool
			candidates.append(new)
			j = j + 1
		i += 1
	return candidates

#Get the support from a list and a count
def support(l, count):
	return len(l)/float(count)

#Apriori algorithm is based in support(A) >= support(superset(A))
#data must be in a list of lists [[],...,[]]
#The return is in the form of
#[
#[[set], support],
#[[set], support],
#[[set], support],
#]
def aprioriSets(data, minSup, maxSize=False):
	global verbose
	size = 1
	#Here we will store our answer
	ret = list()
	#We will need to know the size of the original set
	count      = len(data)
	#First we make a set for each unique item, it must be sorted to bootstrap the sorted genereration of nextGen
	candidates = sorted(map(lambda e: [e], list(set(itertools.chain(*data)))))
	#We get the support for each of the candidates
	supports   = map(lambda c: supportList(c, data), candidates)
	#We zip the candidates with their supports
	candidateSupports = zip(candidates, supports)
	#While we still have candidates to work on
	while len(candidateSupports) > 0:
		if verbose:
			print "Filtering candidates\t\tset size(" + str(size) + ")\tpop size(" + str(len(candidateSupports)) + ")"
		#Filter out those with a support that is too small
		validCandidates   = filter(lambda e: support(e[1], count) >= minSup, candidateSupports)
		#Add the valid candidates to our return
		ret = ret + map(lambda c: [c[0], support(c[1], count)], validCandidates)
		#If we would compute a size out of the scope
		if maxSize and size >= maxSize:
			break
		if verbose:
			print "Building next generation\tset size(" + str(size) + ")\tpop size(" + str(len(validCandidates)) + ")"
		#Make the next generation of candidates
		candidateSupports = aprioriSetGen(validCandidates)
		size += 1
	return ret
