#!/usr/bin/env python
# -*- coding: utf8 -*-
import csv
import collections
import scipy
import datetime
import aprioriset

def readData(fileName):
    # Courses will be tuples with this names
    Course = collections.namedtuple('Course', 'period code name credits grade')
    # Students will be tuples with this names
    Student = collections.namedtuple('Student', 'registration courses')
    ret = list()
    with open(fileName, 'rb') as dataFile:
        # We consider it to be a csv using space as separator
        datum = list(csv.reader(dataFile, delimiter=' '))
        for d in datum:
            # We perform a sanity check to ensure each course has 5 fields
            assert((len(d)-1) % 5 == 0)
            courses = list()
            for i in range((len(d) -1) / 5):
                # Every 5 entries make a record
                # TODO build objects from the values (dates, integers and so on)
                #: courses.append(Course(*d[i*5+1:i*5+6]))
                courses.append(Course(datetime.datetime.strptime(d[i*5+1], '%Y-%m').date(), d[i*5+2], d[i*5+3], float(d[i*5+4]), float(d[i*5+5])))
            ret.append(Student(datetime.datetime.strptime(d[0], '%Y').date(), courses))
    return ret

datum = readData('data.txt')


# (1)Get unique course codes
courseCodes = set(reduce(lambda x,y: x+y, map(lambda student : map(lambda course: course.code, student.courses), datum)))


def countStudentsPassing(datum, courseNames, passing={True, False}):
    if not isinstance(courseNames, collections.Iterable):
        courseNames = list(courseNames)
    # Transform each student by taking only the courses with the given name(s) and passing grade(s)
    return len(filter(lambda s: len(filter(lambda c: (c.grade > 0) in passing and c.name in courseNames, s.courses)), datum))

# Grades will be inclusive


def countStudentsGradeRange(datum, courseNames, minGrade=0, maxGrade=5):
    if not isinstance(courseNames, collections.Iterable):
        courseNames = list(courseNames)
    return len(filter(lambda x: len(filter(lambda y: y.name in courseNames and y.grade >= minGrade and y.grade <= maxGrade, x.courses)) > 0, datum))


def averageGradeStudent(courses):
    # map each course to a vector containing [grade*credits, credits]
    # then we reduce it using the sum
    v = reduce(lambda a,b: [a[0]+b[0], a[1]+b[1] ] ,map(lambda c: [c.grade*c.credits, float(c.credits)], courses), [0, 0])
    if (v[1] == 0):
        return -1
    return v[0]/v[1]

def repeatStudent(courses):
    repeated = list()
    #print courses
    codes = list()
    for course in courses:
        codes.append(course.code)

    uniqueCodes = set(codes)
    for code in uniqueCodes: 
        tries = len(filter(lambda course: course.code == code, courses))        
        if tries>1:
            repeated.append(course)

    return len(repeated)
        

def repeat(students):
    #We check each student for repetitions
    return map(lambda student: repeatStudent(student.courses), students)

def averageGrade(students):
    # We map each student to his average grade and compute the mean of that
    return scipy.mean(filter(lambda grade: grade >= 0, map(lambda student: averageGradeStudent(student.courses), students)))

# (2)
print "Students attempted Introduction to programming:\t" + str(countStudentsPassing(datum, 'Ohjelmoinnin perusteet')) #1200

# (3)
print "Students passed Introduction to programming:\t" + str(countStudentsPassing(datum, 'Ohjelmoinnin perusteet', {True}))

# (4)
print "Students passed Advanced programming:\t" + str(countStudentsPassing(datum, ('Ohjelmoinnin jatkokurssi', 'Java­ohjelmointi'), {True}))

# (5)
print "Students passed Data structures with grade 4 or 5:\t" + str(countStudentsGradeRange(datum, 'Tietorakenteet ja algoritmit', 4, 5))

# Average of all students
print averageGrade(datum)

# Average grade of people who took each course before 2010
print "Average grade of students who took Introduction to programming prior 2010"
print averageGrade(filter(lambda student: len(filter(lambda course: course.name == "Ohjelmoinnin perusteet" and course.period < datetime.datetime.strptime("2010", '%Y').date(), student.courses)) > 0, datum))

# Average grade of people who took each course after 2010
print "Average grade of students who took Introduction to programming on or after 2010"
print averageGrade(filter(lambda student: len(filter(lambda course: course.name == "Ohjelmoinnin perusteet" and course.period >= datetime.datetime.strptime("2010", '%Y').date(), student.courses)) > 0, datum))


def countStudentsTakenCourseXinTimeYwithGradeZ(datum, start_year, end_year, course_name, min_grade, max_grade,):
    result = len(filter(lambda student: start_year <= student.registration.year <= end_year and len(filter(lambda x: course_name == x.name and min_grade <= x.grade <= max_grade, student.courses)), datum))
    return result

print 'Students passed advanced programming after introduction to programming after 2010'

print len(filter(lambda student: student.registration.year >= 2010 and len(filter(lambda x: x.name in ['Ohjelmoinnin jatkokurssi', 'Java­ohjelmointi'] and x.grade > 0 , student.courses)) > 0
                 and len(filter(lambda x: x.name == 'Ohjelmoinnin perusteet' and x.grade > 0, student.courses)) > 0, datum))

print 'Students passed advanced programming after introduction to programming before 2010'

print len(filter(lambda student: student.registration.year < 2010 and len(filter(lambda x: x.name in ['Ohjelmoinnin jatkokurssi', 'Java­ohjelmointi'] and x.grade > 0 , student.courses)) > 0
                 and len(filter(lambda x: x.name == 'Ohjelmoinnin perusteet' and x.grade > 0, student.courses)) > 0, datum))


#How many students had to repeat courses & how many times
nOfRepeats = repeat(filter(lambda student: len(filter(lambda course: course.name == "Ohjelmoinnin perusteet"  and course.period >= datetime.datetime.strptime("2010", '%Y').date(), student.courses)) > 0 and len(filter(lambda course: course.name=="Ohjelmoinnin jatkokurssi" or course.name=="Java-ohjelmointi" and course.period >= datetime.datetime.strptime("2010", '%Y').date(), student.courses))>0, datum))

repeats = set(nOfRepeats)
amount = list()
print "After change"
for instance in repeats:
    print nOfRepeats.count(instance), " students had to repeat ",instance," courses."
print len(nOfRepeats), " students participated."
numOfRepeats = reduce(lambda x, y: x+y, nOfRepeats)
avRepeat = float(numOfRepeats)/len(nOfRepeats)
print "On average ",avRepeat," courses had to be repeated by a single student."

nOfRepeatsPrior =  repeat(filter(lambda student: len(filter(lambda course: course.name == "Ohjelmoinnin perusteet"  and course.period < datetime.datetime.strptime("2010", '%Y').date(), student.courses)) > 0 and len(filter(lambda course: course.name=="Ohjelmoinnin jatkokurssi" or course.name=="Java-ohjelmointi" and course.period < datetime.datetime.strptime("2010", '%Y').date(), student.courses))>0 , datum))


repeats = set(nOfRepeatsPrior)
amount = list()

print "Before change"
for instance in repeats:
    print nOfRepeatsPrior.count(instance), " students had to repeat ",instance," courses. Before change."

print len(nOfRepeatsPrior), " students participated."
avRepeat = float(numOfRepeats)/len(nOfRepeatsPrior)
print "On average ",avRepeat," courses had to be repeated by a single student."

#How many students continued to advanced programming after introduction
#nOfContinuers = (filter(lambda student: len(filter(lambda course: course.name == "Ohjelmoinnin perusteet"  and course.period >= datetime.datetime.strptime("2010", '%Y').date(), student.courses)) > 0, datum)
