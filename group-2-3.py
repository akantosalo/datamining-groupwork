#!/usr/bin/env python
# -*- coding: utf8 -*-
import csv
import collections
import scipy
import datetime
import apriori
import sys
import itertools

def readData(fileName):
    # Courses will be tuples with this names
    Course = collections.namedtuple('Course', 'period code name credits grade')
    # Students will be tuples with this names
    Student = collections.namedtuple('Student', 'registration courses')
    ret = list()
    with open(fileName, 'rb') as dataFile:
        # We consider it to be a csv using space as separator
        datum = list(csv.reader(dataFile, delimiter=' '))
        for d in datum:
            # We perform a sanity check to ensure each course has 5 fields
            assert((len(d)-1) % 5 == 0)
            courses = list()
            for i in range((len(d) -1) / 5):
                # Every 5 entries make a record
                # TODO build objects from the values (dates, integers and so on)
                #: courses.append(Course(*d[i*5+1:i*5+6]))
                courses.append(Course(datetime.datetime.strptime(d[i*5+1], '%Y-%m').date(), d[i*5+2], d[i*5+3], float(d[i*5+4]), float(d[i*5+5])))
            ret.append(Student(datetime.datetime.strptime(d[0], '%Y').date(), courses))
    return ret

datum = readData('data.txt')

#CourseNames
courseNames = dict(                                    #Make a dictionary
	set(itertools.chain(*                              #Of the flatlist
	map(                                               #Of maping
		lambda student: set(map(                       #Each student to the set
			lambda course: (course.code, course.name), #Of pairs (code, name)
			student.courses)),                         #For each course
		datum                                          #For each student
))))


#Delivery 2, question 3
#All enrolments count
coursesTaken = map(
	lambda student: list(set(map(   #Make them unique
		lambda course: course.code, #Get the code
			student.courses         #For each course
		))),
	datum #For each student
)

#Only passing counts
coursesPassed = map(
	lambda student: list(set(map(                     #Make course codes unique
		lambda course: course.code,                   #Get course.code
			filter(
				lambda coursef: coursef.grade >= 1.0, #Take courses with grade bigger than 1.0
				student.courses                       #For each course
				)))),
	datum #For each student
)

#Nicely print the supported sets
def printNiceSets(supportedSets, courseType, courseNames):
	print "----------" + courseType + " courses----------"
	msize = reduce(max, map(lambda s: len(s[0]), supportedSets))
	print "minSup for taken courses (10-item set):" + str(sorted(filter(lambda c: len(c[0]) == msize, supportedSets), key=lambda k: k[1])[-1][1])
	for s in range(1, msize+1):
		f = sorted(filter(lambda c: len(c[0]) == s, supportedSets), key=lambda k: k[1])[-1]
		print str(s) + "-item set with max support is: " + str(map(lambda c: courseNames[c], f[0])) + " and has a support of " + str(f[1])

delta  = .8
minSup = 1.
supportedSets = []
while supportedSets == [] or reduce(max, map(lambda s: len(s[0]), supportedSets)) < 10:
	supportedSets = apriori.aprioriSets(coursesTaken, minSup, 10)
	minSup *= delta
printNiceSets(supportedSets, "Taken", courseNames)

#For sure, minSup will be less than or equal to the prev one, so we keep it
supportedSets = apriori.aprioriSets(coursesPassed, minSup, 10)
while supportedSets == [] or reduce(max, map(lambda s: len(s[0]), supportedSets)) < 10:
	supportedSets = apriori.aprioriSets(coursesPassed, minSup, 10)
	minSup *= delta
printNiceSets(supportedSets, "Passed", courseNames)

